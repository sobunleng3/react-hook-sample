import axios from "axios";
import React from "react";
import { useState, useEffect } from "react";
import PhotoCard from "../components/PhotoCard";

export const AllPhotos = () => {
  const [photos, setPhotos] = useState([]);
  useEffect(() => {
    // to do here
    axios
      .get("https://jsonplaceholder.typicode.com/photos")
      .then((response) => setPhotos(response.data))
      .catch((err) => console.error("Error :", err));
  }, []);

  return (
    <div className="container">
        <h1> All Photos</h1>
      <div className="row">
        {
            photos.map((item)=>
                <div className="col-4">
                    <PhotoCard item ={item}/>
                </div>
            )
        }
      </div>
    </div>
  );
};
